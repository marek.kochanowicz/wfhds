(cl:in-package #:wfhds)


(defun table-name (core demo-id)
  (make-symbol (format nil "~a_~a" core demo-id)))


(defvar *weapon-type-intervals*
  (dict 1 64 ;pistol
        2 32 ;rifle
        3 32 ;smg
        4 384 ;sniper
        ))


(defvar *weapon-types*
  (dict 1 1
        2 1
        3 1
        4 1
        5 1
        6 1
        7 1
        8 1
        9 1
        10 1
        101 3
        102 3
        103 3
        104 3
        105 3
        106 3
        107 3
        201 6
        202 6
        203 6
        204 6
        205 5
        206 5
        301 2
        302 2
        303 2
        304 2
        305 2
        306 4
        307 2
        308 2
        309 4
        310 4
        311 4
        401 9
        402 10
        403 10
        404 10
        405 7
        406 10
        407 10
        501 8
        502 8
        503 8
        504 8
        505 8
        506 8))


(defparameter *target-columns*
  '(target-x target-y target-z target-pitch target-yaw target-vel0
    target-vel1 target-vel2 target-armor target-health target-boolean-mask
    target-zoom-level))


(defparameter *attacker-columns*
  '(playerhurt-id attacker-steam-id attacker-x attacker-y attacker-z attacker-pitch
    attacker-yaw health-dmg armor-dmg hitgroup attacker-side target-side target-steam-id
    attacker-vel0 attacker-vel1 attacker-vel2
    attacker-armor attacker-health attacker-boolean-mask attacker-zoom-level))


(defun gather-positions (demo-id)
  (vellum:with-header ((vellum.header:make-header 'vellum.header:standard-header
                                                  'steam-id 'tick 'x 'y 'z 'pitch 'yaw
                                                  'target-vel0 'target-vel1 'target-vel2
                                                  'target-armor 'target-health 'target-boolean-mask
                                                  'zoom-level))
  (~> (vellum-postmodern:postgres-query
       `(:select steam64_id current_tick x y z pitch yaw
          vec_velocity_0 vec_velocity_1 vec_velocity_2
          armor_value health boolean_mask zoom_level
         :from ,(table-name "demos_demoplayerposition" demo-id))
       vellum.header:*header*)
      (cl-ds.alg:group-by :key (vellum:brr steam-id))
      (cl-ds.alg:on-each (vellum:brr tick x y z
                                     pitch yaw
                                     target-vel0 target-vel1 target-vel2
                                     target-armor target-health
                                     target-boolean-mask
                                     zoom-level))
      (cl-ds.alg:to-vector
       :after (lambda (vector)
                (cl-ds:make-from-traversable vector
                                             'cl-ds.dicts.skip-list:mutable-skip-list-dictionary
                                             #'> #'=))))))


(defun find-target-position (positions tick steam)
  (when (eq :null steam)
    (return-from find-target-position
      #1=(make-list 11 :initial-element :null)))
  (let ((dict (cl-ds:at positions steam)))
    (when (null dict)
      (return-from find-target-position #1#))
    (bind (((:values key found value) (cl-ds:lower-bound dict tick)))
      (declare (ignore key))
      (if found value #1#))))


(defun find-attacker-position (positions tick steam)
  "returns vel0 vel1 vel2 armor health boolean-mask zoom-level"
  (when (eq :null steam)
    (return-from find-attacker-position
      #1=(make-list 7 :initial-element :null)))
  (let ((dict (cl-ds:at positions steam)))
    (when (null dict)
      (return-from find-attacker-position #1#))
    (bind (((:values key found value) (cl-ds:lower-bound dict tick)))
      (declare (ignore key))
      (if found (subseq value 5) #1#))))


(defun gather-rounds (demo-id)
  (vellum:with-header ((vellum:make-header 'vellum:standard-header
                                           'round-order 'terrorists 'counter-terrorists))
    (~> (vellum-postmodern:postgres-query
         `(:select round_order terrorist_steam64_ids counter_terrorist_steam64_ids
            :from ,(table-name "demos_demoround" demo-id))
         vellum.header:*header*)
        (cl-ds.alg:to-hash-table
         :hash-table-key (vellum:brr round-order)
         :hash-table-value (vellum:bind-row (terrorists counter-terrorists)
                             (list
                              (cl-ds:make-from-traversable terrorists
                                                           'cl-ds.sets.skip-list:mutable-skip-list-set
                                                           #'< #'=)
                              (cl-ds:make-from-traversable counter-terrorists
                                                           'cl-ds.sets.skip-list:mutable-skip-list-set
                                                           #'< #'=)))))))


(defun side (rounds round-order steam-id)
  (declare (optimize (debug 3)))
  (if (eq :null steam-id)
      :null
      (bind (((terrorists counter-terrorists) (gethash round-order rounds)))
        (cond ((cl-ds:at terrorists steam-id) 2)
              ((cl-ds:at counter-terrorists steam-id) 3)
              ((< (cl-ds:size terrorists) (cl-ds:size counter-terrorists)) 2)
              (t 3)))))


(defun gather-wfhds (positions rounds demo-id)
  (vellum:with-header ((vellum:make-header 'vellum:standard-header
                                           'steam 'id 'tick 'round 'weapon-id
                                           'attacker-x 'attacker-y 'attacker-z
                                           'attacker-pitch 'attacker-yaw 'health-dmg 'armor-dmg
                                           'hitgroup 'target-steam 'playerhurt-id))
    (~> (vellum-postmodern:postgres-query
         `(:order-by (:select fire.steam64_id (:as fire.id fire_id)
                       fire.current_tick fire.round_order fire.weapon_id
                       fire.x fire.y fire.z fire.pitch fire.yaw
                       hurt.dmg_health hurt.dmg_armor hurt.hitgroup
                       hurt.steam64_id (:as hurt.id playerhurt_id)
                       :from (:as ,(table-name "demos_weaponfire" demo-id)
                                  fire)
                       :left-join (:as ,(table-name "demos_demoplayerhurt" demo-id)
                                       hurt)
                       :on (:= hurt.weapon_fire_id fire.id))
                     current_tick)
         vellum.header:*header*)
        (cl-ds.alg:only (compose (rcurry #'gethash *weapon-type-intervals*)
                                 (rcurry #'gethash *weapon-types*))
                        :key (vellum:brr weapon-id))
        (cl-ds.alg:on-each (vellum:bind-row (id weapon-id attacker-x attacker-y attacker-z
                                                tick attacker-pitch attacker-yaw
                                                health-dmg steam armor-dmg hitgroup
                                                target-steam playerhurt-id round)
                             (append (list weapon-id tick id playerhurt-id steam
                                           attacker-x attacker-y attacker-z
                                           attacker-pitch attacker-yaw health-dmg armor-dmg hitgroup
                                           (side rounds round steam) (side rounds round target-steam)
                                           target-steam)
                                     (find-attacker-position positions tick steam)
                                     (find-target-position positions tick target-steam))))
        (cl-ds.alg:group-by :key (vellum:brr round))
        (cl-ds.alg:group-by :key (vellum:brr steam))
        (cl-ds.alg:group-by :key (vellum:brr weapon-id))
        (cl-ds.alg:partition-if
         (lambda (previous current)
           (let* ((current-tick (second current))
                  (previous-tick (second previous))
                  (weapon-id (first current))
                  (weapon-type (gethash weapon-id *weapon-types*))
                  (tick-difference (- current-tick previous-tick))
                  (interval (gethash weapon-type *weapon-type-intervals*))
                  (in-interval-p (<= tick-difference interval)))
             in-interval-p)))
        (cl-ds.alg:to-vector :key (lambda (data) (map 'vector (curry #'drop 2) data)))
        (vellum:to-table :columns '(round steam weapon-id data)))))


(defun flatten-wfhds (data-frame demo-id)
  (vellum:pipeline (data-frame)
    (~> (cl-ds.alg:multiplex
         :key (vellum:brr data))
        (cl-ds.alg:multiplex
         :function (lambda (sequence)
                     (~> sequence
                         first-elt
                         first-elt
                         (curry #'list* demo-id _)
                         (cl-ds.alg:zip (cl-ds:iota-range)
                                        sequence))))
        (vellum:to-table
         :columns `(demo-id sequence-id sequence-number weapon-fire-id
                            ,@*attacker-columns*
                            ,@*target-columns*)
         :restarts-enabled nil))))


(defun obtain-data (demo-id)
  (~> (gather-wfhds (gather-positions demo-id)
                    (gather-rounds demo-id)
                    demo-id)
      (flatten-wfhds demo-id)))


(defun load-data (data-frame demo-id)
  cl-ds.utils:todo)


(defun run (demo-id)
  (~> (obtain-data demo-id)
      (load-data demo-id)))
