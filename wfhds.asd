(asdf:defsystem #:wfhds
  :name "wfhds"
  :version "0.0.0"
  :author "Marek Kochanowicz"
  :depends-on (#:alexandria
               #:postmodern
               #:iterate
               #:metabang-bind
               #:cl-data-structures
               #:vellum
               #:cl+ssl
               #:vellum-postmodern)
  :serial T
  :pathname "source"
  :components ((:file "aux-package")
               (:file "package")
               (:file "implementation")))
